# kernel

Spring boot project (Java 11)

## Setup Development environment

- Open pom.xml as a project (IntelliJ)
- install dependencies (execute  `mvn install`)
- add your ip to remote mysql access in heliohost.org (cr1571an/admin123)
- if db location changes change spring.datasource props
- Run project (mvn plugins -> springboot -> run)

## Build

execute `mvn package` it should generate a .war file under target dir

## Deployment

It can be deployed as any java web app. We can use websphere, weblogic, tomcat etc.
To deploy in a tomcat app server:
- place the .war in $CATALINA_HOME/webapps/ dir
- run server $CATALINA_HOME/bin/startup.sh
