package com.soa.filesystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Clase para correr el kernel.
 */
@SpringBootApplication
public class KernelApplication {

	public static void main(String[] args) {
		SpringApplication.run(KernelApplication.class, args);
	}

}
