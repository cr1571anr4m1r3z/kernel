package com.soa.filesystem.service;

import com.soa.filesystem.data.dao.UserMapper;
import com.soa.filesystem.data.entities.User;
import com.soa.filesystem.kernel.filesystem.FileSystem;
import com.soa.filesystem.kernel.utils.StrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Clase de servicios de usuario para registar y autenticar.
 */
@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    FileSystem fileSystem;

    public void registerUser(User user) throws Exception {
        log.info("registering user {} {} {}", user.getUserName(), user.getPassword(), user.getHome());
        this.userMapper.registerUser(user);
        if (StrUtils.isEmpty(user.getHome())) {
            user.setHome("/home/" + user.getUserName());
        }
        log.info("user successfully registered. Home {}", user.getHome());
        try {
            this.fileSystem.changeDir(user.getHome());
        } catch(Exception e) {
            log.info("creating user home for user {}", user.getHome());
            this.fileSystem.createDir(user.getHome(), user.getUserName());
        }
    }

    public String authenticate(User user) throws Exception {
        log.info("doing user auth for user {}", user.getUserName());
        User u = this.userMapper.loadUserByNameAndPwd(user.getUserName(), user.getPassword());
        if (u == null) {
            throw new Exception("Invalid credentials or non existing user");
        }
        if (StrUtils.isEmpty(u.getHome())) {
            u.setHome("/home/" + u.getUserName());
        }
        log.info("loaded user {} {}", u.getUserName(), u.getHome());
        try {
            this.fileSystem.changeDir(u.getHome());
        } catch(Exception e) {
            this.fileSystem.createDir(u.getHome(), u.getUserName());
        }
        return u.getHome();
    }

    private static final Logger log = LoggerFactory.getLogger(FileSystem.class);
}
