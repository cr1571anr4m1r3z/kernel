package com.soa.filesystem.service;

import com.soa.filesystem.kernel.command.Command;
import com.soa.filesystem.kernel.command.CommandFactory;
import com.soa.filesystem.kernel.helpers.Cmd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Clase de servicios de kernel para ejecutar comandos de cmd.
 */
@Service
public class KernelService {

    @Autowired
    private CommandFactory commandFactory;

    public String executeCommand(Cmd cmd) throws Exception {
        Command command =  commandFactory.getCommand(cmd.getCmdName());
        command.setExecutorUserName(cmd.getUserName());
        return command.processCmd(cmd.getCmdArgs());
    }
}
