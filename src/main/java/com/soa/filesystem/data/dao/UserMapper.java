package com.soa.filesystem.data.dao;

import com.soa.filesystem.data.entities.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
/**
 * Interfaz para el mapeo de nuevos usuarios.
 */
@Mapper
public interface UserMapper {

    User loadUserByNameAndPwd(@Param("userName") String userName, @Param("password") String password);

    void registerUser(User user);
}
