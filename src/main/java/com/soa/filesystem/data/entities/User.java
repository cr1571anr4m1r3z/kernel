package com.soa.filesystem.data.entities;

import java.io.Serializable;

/**
 * Clase utilizada para setear parámetros del usuario.
 */
public class User implements Serializable {

    private int userId;
    private String userName;
    private String password;
    private String home;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
