package com.soa.filesystem.kernel.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 *Clase con todas las constantes del proyecto.
 */
public class Constants {

    public static final String LS = "ls";
    public static final String ECHO = "echo";
    public static final String CAT = "cat";
    public static final String CD = "cd";
    public static final String MKDIR = "mkdir";
    public static final String RM = "rm";
    public static final String RMDIR = "rmdir";
    public static final String MV = "mv";
    public static final String CP = "cp";
    public static final String LBL = "lbl";
    public static final String LIL = "lil";

    public static final HashMap<String,String> SUPPORTED_CMD = new LinkedHashMap<String, String>();
    static{
        SUPPORTED_CMD.put(LS, "It's used to view the content of a directory. It receives only one argument <filename>. If not specified defaults to '.'");
        SUPPORTED_CMD.put(ECHO,"It's used to create a file with the given content. It receives two arguments <content> <filename>");
        SUPPORTED_CMD.put(CAT,"It's used to view the content of a file. It receives only one argument <filename>");
        SUPPORTED_CMD.put(CD, "It's used to change of directory. It receives only one argument <dir_name>");
        SUPPORTED_CMD.put(MKDIR,"It's used to create a directory. It receives only one argument <dir_name>");
        SUPPORTED_CMD.put(RM, "It's used to remove a file. It receives only one argument <filename>");
        SUPPORTED_CMD.put(RMDIR, "It's used to remove a directory. It receives only one argument <dir_name>");
        SUPPORTED_CMD.put(MV, "It's used to move a file. It receives two arguments <filename> <new_location>");
        SUPPORTED_CMD.put(CP, "It's used copy a file. It receives two arguments <filename> <new_location>");
        SUPPORTED_CMD.put(LBL, "It is used to view lbl");
        SUPPORTED_CMD.put(LIL, "It is used to view lil");
    }
}
