package com.soa.filesystem.kernel.utils;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Clase con utilidades para crear y modificar archivos.
 */
@Service
public class FileUtils {

    private String fsFileName = "fs.bin";
    private Path path = Paths.get(this.fsFileName);

    public boolean fsExists(){
        File f = new File(this.fsFileName);
        return f.exists();
    }

    public String getFSRecord(int lineNo) throws IOException {
        return Files.readAllLines(path).get(lineNo - 1);
    }

    public void modifyFSRecord(int lineNo, String content) throws IOException {
        List<String> lines = Files.readAllLines(path);
        lines.set(lineNo - 1, content);
        Files.write(path, lines);
    }

    public List<String> getAllFSRecords() throws IOException {
        return Files.readAllLines(Paths.get(this.fsFileName));
    }
}
