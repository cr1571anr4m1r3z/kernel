package com.soa.filesystem.kernel.utils;

import java.util.Scanner;

/**
 * Clase con funciones de strings para revisar paths y nombres de archivos.
 */
public class StrUtils {

    private static Scanner sc = new Scanner(System.in);

    public static boolean isEmpty(String str) {
        return (str == null || str.length() == 0 || str.equals(" "));
    }

    public static String getParentDirPath(String dirName) {
        if (dirName.equals("/")) {return dirName;}
        String tmp = dirName;
        if(tmp.endsWith("/")) {
            tmp = tmp.substring(0, tmp.length() - 1);
        }
        int i = tmp.lastIndexOf("/");
        tmp =  dirName.substring(0, i);
        return (isEmpty(tmp)) ? "/" : tmp;
    }

    public static String getFileNameFromPath(String absoluteFilePath) {
        return absoluteFilePath.substring(absoluteFilePath.lastIndexOf("/") + 1);
    }

    public static String getLastDirNameFromPath(String absoluteDirPath) {
        if (absoluteDirPath.equals("/")) {
            return absoluteDirPath;
        }
        String tmp = absoluteDirPath;
        if (tmp.lastIndexOf("/") == tmp.length())
            tmp = tmp.substring(0, tmp.length() - 1);
        return tmp.substring(tmp.lastIndexOf("/") + 1);
    }
}
