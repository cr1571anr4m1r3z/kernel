package com.soa.filesystem.kernel.filesystem.entities;


import com.soa.filesystem.kernel.filesystem.entities.Inode;

/**
 * Clase para setear nombre e inodo, y obtener dichos valores
 */
public class DirEntry {

    private String name;
    private Inode inode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Inode getInode() {
        return inode;
    }

    public void setInode(Inode inode) {
        this.inode = inode;
    }
}
