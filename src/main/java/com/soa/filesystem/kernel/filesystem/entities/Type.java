package com.soa.filesystem.kernel.filesystem.entities;

/**
 * Clase para setear y obtener el tipo de archivo.
 */
public enum Type {
    AVAILABLE(0),
    FILE(1),
    DIRECTORY(2);

    private int type;

    Type(int type) {
        this.type = type;
    }

    public int type() {
        return this.type;
    }
}
