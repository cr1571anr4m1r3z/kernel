package com.soa.filesystem.kernel.filesystem;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.soa.filesystem.kernel.filesystem.entities.DataBlock;
import com.soa.filesystem.kernel.filesystem.entities.DirDataBlock;
import com.soa.filesystem.kernel.filesystem.entities.FileDataBlock;
import com.soa.filesystem.kernel.filesystem.entities.Inode;
import com.soa.filesystem.kernel.filesystem.entities.DirEntry;
import com.soa.filesystem.kernel.filesystem.entities.Type;
import com.soa.filesystem.kernel.utils.FileUtils;
import com.soa.filesystem.kernel.utils.StrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

/**
 * Clase de file system, la cual realiza el procesamiento necesario para el funcionamiento de nuestro servidor de archivos.
 */
@Service
public class FileSystem {

    private static final Logger log = LoggerFactory.getLogger(FileSystem.class);

    @Autowired
    private FileUtils fileUtils;

    private final int INODE_IDX_MIN = 2;
    private final int INODE_IDX_MAX = 258;
    private final int BLOCK_IDX_MIN = 259;
    private final int BLOCK_IDX_MAX = 1283;
    private Map<Integer, Inode> inodeMap;
    private Map<Integer, DataBlock> blockMap;
    private final int LBL_SIZE = 256;
    private final int LIL_SIZE = 32;
    private final int L_MAX_SIZE_TO_SHOW = 10;
    private final int BLOCK_FILE_SIZE = 10;

    private Deque<Integer> lbl;
    private Deque<Integer> lil;

    private ObjectMapper mapper;

    FileSystem() throws Exception {
        this.mapper = new ObjectMapper();
        this.inodeMap = new HashMap<>();
        this.blockMap = new HashMap<>();
        this.lbl = new ArrayDeque<>();
        this.lil = new ArrayDeque<>();
        //if (fileUtils.fsExists()) {
        //    this.loadFileSystem();
        //} else {
            this.initFileSystem();
        //}
    }

    private void loadFileSystem() throws IOException {
        log.info("Loading FileSystem!");
        List<String> lines = this.fileUtils.getAllFSRecords();
        int offset = this.INODE_IDX_MIN;
        // load inodes
        for (int i=this.INODE_IDX_MIN ; i <= this.INODE_IDX_MAX - offset ; i++) {
            Inode inode =  mapper.readValue(lines.get(i - offset), Inode.class);
            this.inodeMap.put(i, inode);
        }
        // load data blocks
        for (int i = this.BLOCK_IDX_MIN; i <= this.BLOCK_IDX_MAX; i++) {
            DataBlock dataBlock = mapper.readValue(lines.get(i - offset), DataBlock.class);
            this.blockMap.put(i, dataBlock);
        }
        log.info("lbl {} ", this.lbl);
        log.info("lil {} ", this.lil);
        log.info("Filesystem loaded!");
    }

    /**
     * Metodo para inicializar el sistema de archivos.
     * @throws Exception
     */
    public void initFileSystem() throws Exception {
        log.info("Initializing File System");
        // init inodes
        int offset = this.INODE_IDX_MIN;
        for (int i = this.INODE_IDX_MIN; i <= this.INODE_IDX_MAX; i++ ) {
            Inode inode = new Inode();
            inode.setNumber(i);
            inode.setType(Type.AVAILABLE);
            this.inodeMap.put(i, inode);
            if (i - offset < this.LIL_SIZE) {
                this.lil.add(i);
            }
        }
        // init data blocks
        offset = this.BLOCK_IDX_MIN;
        for (int i = this.BLOCK_IDX_MIN; i <= this.BLOCK_IDX_MAX; i++ ) {
            DataBlock dataBlock = new DataBlock();
            dataBlock.setNumber(i);
            this.blockMap.put(i, dataBlock);
            if (i - offset < this.LBL_SIZE) {
                this.lbl.add(i);
            }
        }
        log.info("lbl {} ", this.lbl);
        log.info("lil {} ", this.lil);
        // create / dir and assign it to root
        this.createDir("/", "root");
        // create /home dir and assign it to root
        this.createDir("/home", "root");
        log.info("File System Initialized!");
    }

    /**
     * Metodo para crear directorio en el sistema de archivos.
     * @param absoluteDirPath
     * @param owner
     * @throws Exception
     */
    public void createDir(String absoluteDirPath, String owner) throws Exception {
        Inode parent = getInodeByPath(StrUtils.getParentDirPath(absoluteDirPath)); // validate dir can be created before getting an inode
        int idx = getAvailableInode();
        Inode inode = this.inodeMap.get(idx);
        this.createDirectoryAtInode(inode, absoluteDirPath, owner, parent);
    }

    /**
     * Metodo para crear un archivo.
     * @param absoluteFilePath
     * @param content
     * @param owner
     * @throws Exception
     */
    public void createFile(String absoluteFilePath, String content, String owner) throws Exception {
        Inode parent = getInodeByPath(StrUtils.getParentDirPath(absoluteFilePath)); // just to validate correct path
        Inode inode = this.inodeMap.get(this.getAvailableInode());
        this.createFileAtInode(inode, absoluteFilePath, content, owner, parent);
    }

    /**
     * Metodo utilizado para crear una lista de strings para el comando ls
     * @param path
     * @return
     * @throws Exception
     */
    public String doList(String path) throws Exception {
        StringBuilder sb = new StringBuilder();
        Inode inode = getInodeByPath(path);
        if (inode.getType().equals(Type.FILE)) {
            FileDataBlock fdb = (FileDataBlock) this.blockMap.get(inode.getBlockLinks().get(0)).getDatablock();
            sb.append(inode.getNumber()).append(" \t ").append(this.getTypeChar(inode.getType())).append("\t")
                    .append(inode.getUid()).append(" \t\t ").append(fdb.getName()).append("\n");
        } else if (inode.getType().equals(Type.DIRECTORY)) {
            sb.append(inode.getNumber()).append(" \t ").append(this.getTypeChar(inode.getType())).append("\t")
                    .append(inode.getUid()).append(" \t\t . ").append("\n");
            DirDataBlock dirDataBlock = (DirDataBlock) this.blockMap.get(inode.getBlockLinks().get(0)).getDatablock();
            Inode parentInode = dirDataBlock.getParentInode();
            sb.append(parentInode.getNumber()).append(" \t ").append(this.getTypeChar(parentInode.getType()))
                    .append("\t").append(parentInode.getUid()).append(" \t\t .. ").append("\n");
            for (DirEntry child : dirDataBlock.getChildren()) {
                Inode childInode = child.getInode();
                sb.append(childInode.getNumber()).append(" \t ").append(this.getTypeChar(childInode.getType())).append("\t").append(childInode.getUid()).append(" \t\t ").append(child.getName()).append("\n");
            }
        }
        return sb.toString();
    }

    /**
     * Metodo para obtener el tipo de archivo
     * @param type
     * @return
     */
    private String getTypeChar(Type type) {
        if (type.equals(Type.DIRECTORY)) {
            return "d";
        } else {
            return "f";
        }
    }

    /**
     * Metodo para crear un directorio en el inodo.
     * @param inode
     * @param absoluteDirPath
     * @param owner
     * @param parentInode
     * @throws Exception
     */
    private void createDirectoryAtInode(Inode inode, String absoluteDirPath, String owner, Inode parentInode) throws Exception {
        // create dir entry and set current inode and name
        DirEntry dirEntry = new DirEntry();
        dirEntry.setInode(inode);
        if (absoluteDirPath.equals("/")) {
            dirEntry.setName(absoluteDirPath);
        } else {
            String directoryName = StrUtils.getLastDirNameFromPath(absoluteDirPath);
            dirEntry.setName(directoryName);
        }
        // create dir data block
        DirDataBlock dirDataBlock = new DirDataBlock();
        dirDataBlock.setCurrent(dirEntry);
        // set parent inode
        if (absoluteDirPath.equals("/")) {
            dirDataBlock.setParentInode(inode);
        } else {
            Inode parent = getAndUpdateParentMetadata(absoluteDirPath, dirEntry, parentInode);
            dirDataBlock.setParentInode(parent);
        }
        // get and assign block
        int blockNum = getAvailableBlock();
        DataBlock b =  this.blockMap.get(blockNum);
        b.setDatablock(dirDataBlock);
        b.setType(Type.DIRECTORY);
        this.blockMap.put(blockNum, b);
        // update inode metadata
        inode.getBlockLinks().add(b.getNumber());
        inode.setType(Type.DIRECTORY);
        inode.setUid(owner);
    }

    /**
     * Metodo para crear un archivo en el inodo.
     * @param inode
     * @param absoluteFilePath
     * @param content
     * @param owner
     * @param parent
     * @throws Exception
     */
    private void createFileAtInode(Inode inode, String absoluteFilePath, String content, String owner, Inode parent) throws Exception {
        int requiredBlocks = (int) Math.ceil((double) content.length() / 10);
        String fileName = StrUtils.getFileNameFromPath(absoluteFilePath);
        List<Integer> blocks = new ArrayList<>();
        for (int i = 0; i < requiredBlocks; i++) {
            int s = this.BLOCK_FILE_SIZE * i, e = s + this.BLOCK_FILE_SIZE;
            String dataBlockContent = content.substring(s, (e < content.length() ? e : content.length()));
            int num = this.getAvailableBlock();
            blocks.add(num);
            FileDataBlock fileDataBlock = new FileDataBlock();
            fileDataBlock.setNumber(num);
            fileDataBlock.setName(fileName);
            fileDataBlock.setData(dataBlockContent);
            DataBlock dataBlock = new DataBlock();
            dataBlock.setType(Type.FILE);
            dataBlock.setDatablock(fileDataBlock);
            this.blockMap.put(num, dataBlock);
        }
        inode.setBlockLinks(blocks);
        inode.setUid(owner);
        inode.setType(Type.FILE);
        // create dir entry to update parent inode
        DirEntry dirEntry = new DirEntry();
        dirEntry.setInode(inode);
        dirEntry.setName(fileName);
        getAndUpdateParentMetadata(absoluteFilePath, dirEntry, parent);
    }

    /**
     * Metodo para obtener un bloque disponible.
     * @return
     */
    private int getAvailableBlock() {
        int bl = lbl.poll();
        if (this.lbl.isEmpty())  {
            int size = 0;
            int currentIndex = bl + 1;
            while (size < this.LBL_SIZE && currentIndex < this.BLOCK_IDX_MAX) {
                if (this.blockMap.get(currentIndex).getType().equals(Type.AVAILABLE)) {
                    lbl.addLast(currentIndex);
                    size++;
                }
                currentIndex++;
            }
        }
        return bl;
    }

    /**
     * Metodo para obtener un inodo disponible.
     * @return
     */
    private int getAvailableInode() {
        int il = lil.poll();
        if (this.lil.isEmpty())  {
            int size = 0;
            int currentIndex = il + 1;
            while (size < this.LIL_SIZE && currentIndex < this.INODE_IDX_MAX) {
                if (this.inodeMap.get(currentIndex).getType().equals(Type.AVAILABLE)) {
                    lil.addLast(currentIndex);
                    size++;
                }
                currentIndex++;
            }
        }
        return il;
    }

    private Inode getAndUpdateParentMetadata(String absolutePath, DirEntry children, Inode parent) throws Exception {
        Inode inode;
        if (parent == null) {
            String parentDirname = StrUtils.getParentDirPath(absolutePath);
            inode = getInodeByPath(parentDirname);
        } else {
            inode = parent;
        }
        int blNum = inode.getBlockLinks().get(0);
        DataBlock dataBlock = this.blockMap.get(blNum);
        DirDataBlock dir = (DirDataBlock) dataBlock.getDatablock();
        dir.getChildren().add(children);
        dataBlock.setDatablock(dir);
        this.blockMap.put(blNum, dataBlock);
        return inode;
    }

    private Inode getInodeByPath(String path) throws Exception {
        Inode currentInode = this.inodeMap.get(this.INODE_IDX_MIN);;
        if (!path.equals("/"))  {
            String pathParts[] = path.split("/");
            for (int i = 0; i< pathParts.length; i++) {
                String currentPath = StrUtils.isEmpty(pathParts[i]) ? "/" : pathParts[i];
                List<Integer> blockLinks = currentInode.getBlockLinks();
                for (int j = 0; j< blockLinks.size(); j++) {
                    DataBlock dataBlock = this.blockMap.get(blockLinks.get(j));
                    if (dataBlock.getType().equals(Type.FILE)) {
                        FileDataBlock fileDataBlocks = (FileDataBlock) dataBlock.getDatablock();
                        if (fileDataBlocks != null) {
                            if (fileDataBlocks != null && fileDataBlocks.getName().equals(currentPath)) {
                                return currentInode;
                            }
                        }
                    } else if (dataBlock.getType().equals(Type.DIRECTORY)) {
                        DirDataBlock dirDataBlock = (DirDataBlock) dataBlock.getDatablock();
                        if (dirDataBlock.getCurrent().getName().equals(currentPath)) {
                            currentInode =  dirDataBlock.getCurrent().getInode();
                        } else {
                            for (DirEntry child : dirDataBlock.getChildren()) {
                                if (child.getName().equals(currentPath)) {
                                    currentInode = child.getInode();
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!path.equals("/") && currentInode.getNumber() == this.INODE_IDX_MIN) {
            throw new Exception("Invalid path. Cannot stat "+path);
        }
        if (currentInode.getBlockLinks().size() > 0) {
            String expectedName = StrUtils.getLastDirNameFromPath(path);
            DataBlock cdb = this.blockMap.get(currentInode.getBlockLinks().get(0));
            if (currentInode.getType().equals(Type.DIRECTORY)) {
                DirDataBlock ddb = (DirDataBlock) cdb.getDatablock();
                if (!ddb.getCurrent().getName().equals(expectedName)) {
                    throw new Exception("Invalid path. Cannot stat "+path);
                }
            } else {
                FileDataBlock fdb = (FileDataBlock) cdb.getDatablock();
                if (!fdb.getName().equals(expectedName)) {
                    throw new Exception("Invalid path. Cannot stat "+path);
                }
            }
        }
        return currentInode;
    }

    /**
     * Metodo para obtener contenido del archivo
     * @param absoluteFilePath
     * @return
     * @throws Exception
     */
    public String getFileContent(String absoluteFilePath) throws Exception {
        Inode inode = getInodeByPath(absoluteFilePath);
        if (inode.getType().equals(Type.DIRECTORY)) {
            throw new Exception(absoluteFilePath+" is a directory!");
        }
        List<Integer> blockLinks = inode.getBlockLinks();
        StringBuilder sb = new StringBuilder();
        for (int link : blockLinks) {
            DataBlock dt = this.blockMap.get(link);
            FileDataBlock fdb = (FileDataBlock) dt.getDatablock();
            sb.append(fdb.getData());
        }
        return sb.toString();
    }

    /**
     * Metodo para obtener lista de bloques libres
     * @return
     */
    public List<Integer> getLBL() {
        int c = 0;
        Iterator<Integer> i = this.lbl.iterator();
        List<Integer> list = new ArrayList<>();
        while (i.hasNext() && c++ < this.L_MAX_SIZE_TO_SHOW) {
            list.add(i.next());
        }
        return list;
    }

    /**
     * Metodo para obtener lista de inodos libres
     * @return
     */
    public List<Integer> getLIL() {
        int c = 0;
        Iterator<Integer> i = this.lil.iterator();
        List<Integer> list = new ArrayList<>();
        while (i.hasNext() && c++ < this.L_MAX_SIZE_TO_SHOW) {
            list.add(i.next());
        }
        return list;
    }

    /**
     * Metodo para eliminar archivo
     * @param absolutePath
     * @param userExecutingCmd
     * @throws Exception
     */
    public void removeFile(String absolutePath, String userExecutingCmd) throws Exception {
        Inode inode = this.getInodeByPath(absolutePath);
        if(inode.getType().equals(Type.FILE)){
            if (!inode.getUid().equals(userExecutingCmd)) {
                throw new Exception("User "+userExecutingCmd+" does not have permissions to remove given file "+absolutePath);
            }
            this.removeEntryFromParent(absolutePath);
            // remove the file
            List<Integer> blocks = inode.getBlockLinks();
            //Collections.sort(blocks);
            for (int block : blocks) {
                freeBlock(block);
            }
            freeInode(inode.getNumber());
        } else {
            throw new Exception(absolutePath + " is not a file!");
        }
    }

    /**
     * Metodo para eliminar entrada del directorio padre
     * @param absolutePath
     * @throws Exception
     */
    private void removeEntryFromParent(String absolutePath) throws Exception {
        // remove entry from parent dir
        Inode parentInode = this.getInodeByPath(StrUtils.getParentDirPath(absolutePath));
        int blockN = parentInode.getBlockLinks().get(0);
        DataBlock dt = this.blockMap.get(blockN);
        DirDataBlock dirDataBlock = (DirDataBlock) dt.getDatablock();
        String name = StrUtils.getFileNameFromPath(absolutePath);
        List<DirEntry> childs = dirDataBlock.getChildren();
        for (DirEntry child : childs) {
            if (child.getName().equals(name)) {
                childs.remove(child);
                break;
            }
        }
        dirDataBlock.setChildren(childs);
        dt.setDatablock(dirDataBlock);
        this.blockMap.put(blockN, dt);
    }

    /**
     * Metodo para eliminar directorio especificado
     * @param absolutePath
     * @param userExecutingCmd
     * @throws Exception
     */
    public void removeDirectory(String absolutePath, String userExecutingCmd) throws Exception {
        Inode inode = this.getInodeByPath(absolutePath);
        if (inode.getType().equals(Type.DIRECTORY)) {
            if (!inode.getUid().equals(userExecutingCmd)) {
                throw new Exception("User does not have permissions to remove given file "+absolutePath);
            }
            this.removeEntryFromParent(absolutePath);
            // remove the directory
            List<Integer> blocks = inode.getBlockLinks();
            //Collections.sort(blocks);
            for (int block : blocks) {
                DataBlock dataBlock = this.blockMap.get(block);
                DirDataBlock dirDataBlock = (DirDataBlock) dataBlock.getDatablock();
                if (dirDataBlock.getChildren().size() > 0) {
                    throw new Exception("Directory not empty");
                }
                freeBlock(block);
            }
            freeInode(inode.getNumber());
        } else {
            throw new Exception(absolutePath + " is not aa directory!");
        }
    }

    /**
     * Metodo para liberar bloque
     * @param block
     */
    private void freeBlock(int block) {
        this.blockMap.get(block).setType(Type.AVAILABLE);
        if (this.lbl.size() < this.BLOCK_IDX_MAX) {
            this.lbl.addFirst(block);
        }
    }

    /**
     * Metodo para liberar inodo
     * @param inode
     */
    private void freeInode(int inode) {
        this.inodeMap.get(inode).setType(Type.AVAILABLE);
        this.inodeMap.get(inode).setBlockLinks(new ArrayList<>());
        if (this.lil.peek() > inode) {
            lil.addFirst(inode);
        }
    }

    /**
     * Metodo para cambiar directorio
     * @param absolutePath
     * @return
     * @throws Exception
     */
    public boolean changeDir(String absolutePath) throws Exception {
        getInodeByPath(absolutePath);
        return true;
    }

    /**
     * Metodo para mover archivos de un path a otro
     * @param sourcePath
     * @param destPath
     * @param userPerformingMove
     * @throws Exception
     */
    public void doMove(String sourcePath, String destPath, String userPerformingMove) throws Exception {
        String parentSourcePath = StrUtils.getParentDirPath(sourcePath);
        String parentDestPath = StrUtils.getParentDirPath(destPath);
        Inode src = this.getInodeByPath(sourcePath);
        if(!src.getUid().equals(userPerformingMove)) {
            throw new Exception("User " +userPerformingMove+" is not allowed to move file "+sourcePath);
        }
        if (parentDestPath.equals(parentSourcePath)) {
            // rename
            Inode parentInode = null;
            String oldName = null, newName = null;
            if (src.getType().equals(Type.DIRECTORY)) {
                oldName = StrUtils.getLastDirNameFromPath(sourcePath);
                newName = StrUtils.getLastDirNameFromPath(destPath);
                // update name in current dir entry
                int blockNum = src.getBlockLinks().get(0);
                DataBlock dataBlock = this.blockMap.get(blockNum);
                DirDataBlock dir = (DirDataBlock) dataBlock.getDatablock();
                DirEntry entry = dir.getCurrent();
                entry.setName(newName);
                dir.setCurrent(entry);
                dataBlock.setDatablock(dir);
                this.blockMap.put(blockNum, dataBlock);
                parentInode = dir.getParentInode();
            } else if (src.getType().equals(Type.FILE)) {
                oldName = StrUtils.getFileNameFromPath(sourcePath);
                newName = StrUtils.getFileNameFromPath(destPath);
                // update name in current entry
                for (int blockNum : src.getBlockLinks()) {
                    DataBlock dataBlock = this.blockMap.get(blockNum);
                    FileDataBlock file = (FileDataBlock) dataBlock.getDatablock();
                    file.setName(newName);
                    dataBlock.setDatablock(file);
                    this.blockMap.put(blockNum, dataBlock);
                }
                parentInode = getInodeByPath(StrUtils.getParentDirPath(sourcePath));
            }
            // update name in parent entry too
            int parentBlockNum = parentInode.getBlockLinks().get(0);
            DataBlock parentDataBlock = this.blockMap.get(parentBlockNum);
            DirDataBlock parentDir = (DirDataBlock) parentDataBlock.getDatablock();
            List<DirEntry> childEntries = parentDir.getChildren();
            for (DirEntry child : childEntries) {
                if (child.getName().equals(oldName)) {
                    child.setName(newName);
                }
            }
            parentDir.setChildren(childEntries);
            parentDataBlock.setDatablock(parentDir);
            this.blockMap.put(parentBlockNum, parentDataBlock);
        } else {
            // move
            String name = src.getType().equals(Type.FILE) ? StrUtils.getFileNameFromPath(sourcePath)
                    : StrUtils.getLastDirNameFromPath(sourcePath);
            Inode parentInodeSrc = this.getInodeByPath(StrUtils.getParentDirPath(sourcePath));
            Inode parentInodeDst = this.getInodeByPath(StrUtils.getParentDirPath(destPath));
            if(!parentInodeDst.getUid().equals(userPerformingMove)) {
                throw new Exception("User " +userPerformingMove+" is not allowed to perform move operation to destination dir "+destPath);
            }
            // remove entry from current parent
            int blockNum = parentInodeSrc.getBlockLinks().get(0);
            DataBlock dataBlock = this.blockMap.get(blockNum);
            DirDataBlock dirDataBlock = (DirDataBlock) dataBlock.getDatablock();
            List<DirEntry> childs = dirDataBlock.getChildren();
            DirEntry itemToMove = null;
            for (DirEntry dirEntry : childs) {
                if (dirEntry.getName().equals(name)) {
                    itemToMove = dirEntry;
                    childs.remove(dirEntry);
                    break;
                }
            }
            dirDataBlock.setChildren(childs);
            dataBlock.setDatablock(dirDataBlock);
            this.blockMap.put(blockNum, dataBlock);
            // add entry to new parent
            blockNum = parentInodeDst.getBlockLinks().get(0);
            dataBlock = this.blockMap.get(blockNum);
            dirDataBlock = (DirDataBlock) dataBlock.getDatablock();
            childs = dirDataBlock.getChildren();
            childs.add(itemToMove);
            dirDataBlock.setChildren(childs);
            dataBlock.setDatablock(dirDataBlock);
            this.blockMap.put(blockNum, dataBlock);
        }
    }

    /**
     * Metodo para copiar archivo
     * @param absolutePath
     * @param copyFileName
     * @param userPerformingCopy
     * @throws Exception
     */
    public void copyFile(String absolutePath, String copyFileName, String userPerformingCopy) throws Exception {
        Inode inode = getInodeByPath(absolutePath);
        if (!inode.getUid().equals(userPerformingCopy)) {
            throw new Exception("User " + userPerformingCopy + " is not allowed to perform copy operation on given path ");
        }
        String oldName = StrUtils.getFileNameFromPath(absolutePath);
        if (oldName.equals(copyFileName)) {
            throw new Exception("Name for the copy file cannot be the same as the current file name");
        }
        // perform copy
        String content = getFileContent(absolutePath);
        String path = StrUtils.getParentDirPath(absolutePath) + "/" + copyFileName;
        Inode newFileInode = this.inodeMap.get(this.getAvailableInode());
        this.createFileAtInode(newFileInode, path, content, userPerformingCopy, null);
    }
}

