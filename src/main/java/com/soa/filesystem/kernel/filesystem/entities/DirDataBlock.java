package com.soa.filesystem.kernel.filesystem.entities;


import java.util.ArrayList;
import java.util.List;

/**
 * Clase para obtener direccion actual de data block, padre e hijo.
 */
public class DirDataBlock {

    private int num;
    private Inode parentInode;
    private DirEntry current;
    private List<DirEntry> children;

    public DirDataBlock() {
        this.children = new ArrayList<>();
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Inode getParentInode() {
        return parentInode;
    }

    public void setParentInode(Inode parentInode) {
        this.parentInode = parentInode;
    }

    public DirEntry getCurrent() {
        return current;
    }

    public void setCurrent(DirEntry current) {
        this.current = current;
    }

    public List<DirEntry> getChildren() {
        return children;
    }

    public void setChildren(List<DirEntry> children) {
        this.children = children;
    }
}
