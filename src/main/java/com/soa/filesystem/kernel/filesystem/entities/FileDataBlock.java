package com.soa.filesystem.kernel.filesystem.entities;

import java.io.Serializable;

/**
 * Clase de File Data Bllock para setear y obtener parametros.
 */
public class FileDataBlock implements Serializable {
    private int number;
    private String name;
    private String data;

    public FileDataBlock(){

    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
