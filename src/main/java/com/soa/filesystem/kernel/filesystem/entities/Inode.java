package com.soa.filesystem.kernel.filesystem.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase de inodo para setear y obtener parametros.
 */
public class Inode implements Serializable {

    private int number;
    private Type type; // 0 unused , 1 file, 2 dir
    private List<Integer> blockLinks;
    private String uid;

    public Inode(){
        this.type = Type.AVAILABLE;
        this.blockLinks = new ArrayList<>();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<Integer> getBlockLinks() {
        return blockLinks;
    }

    public void setBlockLinks(List<Integer> blockLinks) {
        this.blockLinks = blockLinks;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
