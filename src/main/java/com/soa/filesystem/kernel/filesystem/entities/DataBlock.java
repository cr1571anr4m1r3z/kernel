package com.soa.filesystem.kernel.filesystem.entities;

/**
 *Clase con metodos del bloque de datos.
 */
public class DataBlock {
    private int number;
    private Type type;// 0 available // 1 file // 2 dir
    private Object datablock;

    public DataBlock(){
        this.type = Type.AVAILABLE;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Object getDatablock() {
        return datablock;
    }

    public void setDatablock(Object datablock) {
        this.datablock = datablock;
    }
}
