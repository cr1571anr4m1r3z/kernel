package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;
/**
 * Clase utilizada para ejecutar el comando mv en terminal.
 */
@Component
public class MvCmd extends Command {

    @Override
    public String processCmd(String... args) throws Exception {
        this.fileSystemService.doMove(args[0], args[1], this.getExecutorUserName());
        return null;
    }
}
