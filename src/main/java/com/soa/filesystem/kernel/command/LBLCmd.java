package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 *Clase que crea la lista de bloques libres.
 */
@Component
public class LBLCmd extends Command{
    @Override
    public String processCmd(String... args) {
        StringBuilder sb = new StringBuilder();
        List<Integer> lbl = this.fileSystemService.getLBL();
        sb.append("[");
        for (int i = 0; i < lbl.size(); i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append(lbl.get(i));
        }
        sb.append("]");
        return sb.toString();
    }
}
