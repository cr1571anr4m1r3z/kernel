package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;

/**
 * Clase utilizada para ejecutar el comando rm en terminal.
 */
@Component
public class RmCmd extends Command {

    @Override
    public String processCmd(String... args) throws Exception {
        this.fileSystemService.removeFile(args[0], this.getExecutorUserName());
        return null;
    }
}
