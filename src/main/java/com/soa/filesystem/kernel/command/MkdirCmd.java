package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;

/**
 * Clase utilizada para ejecutar el comando Mkdir en terminal.
 */
@Component
public class MkdirCmd extends Command{

    @Override
    public String processCmd(String... args) throws Exception {
        this.fileSystemService.createDir(args[0], this.getExecutorUserName());
        return null;
    }
}
