package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;
/**
 * Clase utilizada para obtener el contenido de un archivo con el comando cat.
 */
@Component
public class CatCmd extends Command {

    public String processCmd(String... args) throws Exception {
        return this.fileSystemService.getFileContent(args[0]);
    }


}
