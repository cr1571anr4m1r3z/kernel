package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;
/**
 * Clase utilizada para cambiar la direccion con el comando cd.
 */
@Component
public class CdCmd extends Command {

    @Override
    public String processCmd(String... args) throws Exception {
        this.fileSystemService.changeDir(args[0]);
        return null;
    }
}
