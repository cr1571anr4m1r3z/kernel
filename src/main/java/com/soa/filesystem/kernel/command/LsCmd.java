package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;

/**
 *  Clase utilizada para ejecutar el comando ls en terminal-
 */
@Component
public class LsCmd extends Command {

    public String processCmd(String... args) throws Exception {
        return this.fileSystemService.doList(args[0]);
    }
}
