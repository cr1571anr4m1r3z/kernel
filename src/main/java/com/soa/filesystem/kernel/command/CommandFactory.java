package com.soa.filesystem.kernel.command;


import com.soa.filesystem.kernel.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Clase para setear los comandos de terminal para todas las clases que extienden de la clase Command.
 */
@Service
public class CommandFactory {

    @Autowired
    private CatCmd catCmd;

    @Autowired
    private CdCmd cdCmd;

    @Autowired
    private CpCmd cpCmd;

    @Autowired
    private EchoCmd echoCmd;

    @Autowired
    private LBLCmd lblCmd;

    @Autowired
    private LILCmd lilCmd;

    @Autowired
    private LsCmd lsCmd;

    @Autowired
    private MkdirCmd mkdirCmd;

    @Autowired
    private MvCmd mvCmd;

    @Autowired
    private RmCmd rmCmd;

    @Autowired
    private RmdirCmd rmdirCmd;

    public Command getCommand(String cmdName) throws Exception {
        switch (cmdName.toLowerCase()) {
            case Constants.MV:
                return this.mvCmd;
            case Constants.RM:
                return this.rmCmd;
            case Constants.RMDIR:
                return this.rmdirCmd;
            case Constants.CAT:
                return this.catCmd;
            case Constants.MKDIR:
                return this.mkdirCmd;
            case Constants.LS :
                return this.lsCmd;
            case Constants.ECHO:
                return this.echoCmd;
            case Constants.CD :
                return this.cdCmd;
            case Constants.CP:
                return this.cpCmd;
            case Constants.LBL:
                return this.lblCmd;
            case Constants.LIL:
                return this.lilCmd;
            default:
                throw new Exception("Unrecognized command "+cmdName);
        }
    }
}
