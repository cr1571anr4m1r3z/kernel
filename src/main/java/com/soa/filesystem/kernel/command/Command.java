package com.soa.filesystem.kernel.command;


import com.soa.filesystem.kernel.filesystem.FileSystem;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Clase prototipo para ejecutar comandos de terminal.
 */
public abstract class Command {

    @Autowired
    protected FileSystem fileSystemService;

    private String executorUserName = null;

    public abstract String processCmd(String... args) throws Exception;

    public String getExecutorUserName() {
        return executorUserName;
    }

    public void setExecutorUserName(String executorUserName) {
        this.executorUserName = executorUserName;
    }
}
