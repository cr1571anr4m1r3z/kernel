package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;

/**
 * Clase utilizada para copiar un archivo con el comando cp
 */
@Component
public class CpCmd extends Command {

    @Override
    public String processCmd(String... args) throws Exception {
        this.fileSystemService.copyFile(args[0], args[1], getExecutorUserName());
        return null;
    }
}
