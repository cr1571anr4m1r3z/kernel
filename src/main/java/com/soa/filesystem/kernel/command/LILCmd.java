package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Clase para crear la lista de inodos libres
 */
@Component
public class LILCmd extends Command{

    @Override
    public String processCmd(String... args) {
        StringBuilder sb = new StringBuilder();
        List<Integer> lil = this.fileSystemService.getLIL();
        sb.append("[");
        for (int i = 0; i < lil.size(); i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append(lil.get(i));
        }
        sb.append("]");
        return sb.toString();
    }
}
