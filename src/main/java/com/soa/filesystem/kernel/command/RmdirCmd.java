package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;
/**
 * Clase utilizada para ejecutar el comando rmdir en terminal.
 */
@Component
public class RmdirCmd extends Command {

    @Override
    public String processCmd(String... args) throws Exception {
        this.fileSystemService.removeDirectory(args[0], this.getExecutorUserName());
        return null;
    }
}
