package com.soa.filesystem.kernel.command;

import org.springframework.stereotype.Component;

/**
 * Clase utilizada para realizar el comando echo en terminal.
 */
@Component
public class EchoCmd extends Command {

    @Override
    public String processCmd(String... args) throws Exception {
        this.fileSystemService.createFile(args[1], args[0], this.getExecutorUserName());
        return null;
    }
}
