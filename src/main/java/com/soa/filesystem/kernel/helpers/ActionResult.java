package com.soa.filesystem.kernel.helpers;

import java.io.Serializable;

/**
 * Clase usada para setear resultados de acciones
 */
public class ActionResult implements Serializable {

    private boolean isSuccess;
    private String output;

    public ActionResult(){}

    public ActionResult(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public ActionResult(boolean isSuccess, String output) {
        this.isSuccess = isSuccess;
        this.output = output;
    }

    public boolean isSuccess(){
        return this.isSuccess;
    }

    public void setIsSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public static ActionResult SUCC(){
        return new ActionResult(true);
    }

    public static ActionResult SUCC(String output){
        return new ActionResult(true, output);
    }

    public static ActionResult FAIL(){
        return new ActionResult(false);
    }

    public static ActionResult FAIL(String output){
        return new ActionResult(false, output);
    }
}
