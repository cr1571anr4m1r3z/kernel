package com.soa.filesystem.kernel.helpers;
import java.io.Serializable;

/**
 * Clase usada para setear los parametros de la terminal.
 */
public class Cmd implements Serializable {

    private String cmdName;
    private String[] cmdArgs;
    private String userName;

    public Cmd(){}

    public Cmd(String cmdName, String[] cmdArgs) {
        this.cmdName = cmdName;
        this.cmdArgs = cmdArgs;
    }

    public String getCmdName() {
        return cmdName;
    }

    public String[] getCmdArgs() {
        return cmdArgs;
    }

    public void setCmdName(String cmdName) {
        this.cmdName = cmdName;
    }

    public void setCmdArgs(String[] cmdArgs) {
        this.cmdArgs = cmdArgs;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
