package com.soa.filesystem.ctrl;

import com.soa.filesystem.data.entities.User;
import com.soa.filesystem.kernel.helpers.ActionResult;
import com.soa.filesystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * Clase para el control de usuarios, ya sea para registro de nuevos usuarios o login de un usuario previamente creado.
 */
@CrossOrigin
@Controller
@RequestMapping("user")
public class UserCtrl {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ActionResult registerUser(@RequestBody User user){
        try {
            this.userService.registerUser(user);
            return ActionResult.SUCC();
        } catch(Exception e) {
            e.printStackTrace();
            return ActionResult.FAIL(e.getMessage());
        }
    }

    @PostMapping("/login")
    public ActionResult doUserAuthentication(@RequestBody User user){
        try {
            String home = this.userService.authenticate(user);
            return ActionResult.SUCC(home);
        } catch(Exception e) {
            e.printStackTrace();
            return ActionResult.FAIL(e.getMessage());
        }
    }
}
