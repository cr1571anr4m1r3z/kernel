package com.soa.filesystem.ctrl;

import com.soa.filesystem.kernel.helpers.ActionResult;
import com.soa.filesystem.kernel.helpers.Cmd;
import com.soa.filesystem.service.KernelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Clase para ejecutar cmd y comandos de terminal.
 */
@CrossOrigin
@RestController
@RequestMapping("kernel")
public class KernelCtrl {

    @Autowired
    private KernelService kernelService;

    @PostMapping("/executeCmd")
    public @ResponseBody ActionResult executeCmd(@RequestBody Cmd cmd) {
        try {
            return ActionResult.SUCC(this.kernelService.executeCommand(cmd));
        } catch(Exception e) {
            System.out.println("Error while processing cmd : "+cmd.getCmdName());
            e.printStackTrace();
            return ActionResult.FAIL(e.getMessage());
        }
    }
}
