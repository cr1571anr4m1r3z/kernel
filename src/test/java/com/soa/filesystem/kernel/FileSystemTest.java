package com.soa.filesystem.kernel;

/**
 * Clase utilizada para probar el sistema de archivos.
 */
public class FileSystemTest {

    public static void main(String[] args) {
        /* create /home/cristian
        this.createDir("/home/cristian", "cristian");
        this.createDir("/home/gaby", "gaby");
        this.createDir("/home/juan", "juan");
        this.createDir("/home/gaby/myDocs", "gaby");
        this.createFile("/home/gaby/hola.txt", "hola mundo estoy queriendo escribir mas de 10 chars","gaby");
        // ls
        System.out.println("====PERFORMING LS======");
        System.out.println(this.doList("/home/gaby/hola.txt"));
        System.out.println("====PERFORMING CAT======");
        System.out.println(this.getFileContent("/home/gaby/hola.txt"));
        System.out.println("====PERFORMING COPY======");
        this.copyFile("/home/gaby/hola.txt", "holacopy.txt", "gaby");
        System.out.println("====PERFORMING LS======");
        System.out.println(this.doList("/home/gaby"));
        System.out.println("====PERFORMING RENAME======");
        this.doMove("/home/gaby/holacopy.txt", "/home/gaby/holarenamed.txt", "gaby");
        System.out.println("====PERFORMING LS======");
        System.out.println(this.doList("/home/gaby"));
        System.out.println("====PERFORMING MOVE======");
        this.doMove("/home/gaby/holarenamed.txt", "/home/gaby/myDocs/holarenamed.txt", "gaby");
        System.out.println("====PERFORMING LS======");
        System.out.println(this.doList("/home/gaby"));
        System.out.println("====PERFORMING LS======");
        System.out.println(this.doList("/home/gaby/myDocs"));
        System.out.println("====PERFORMING CAT======");
        System.out.println(this.getFileContent("/home/gaby/myDocs/holarenamed.txt"));
        System.out.println("====PERFORMING RM======");
        this.removeFile("/home/gaby/myDocs/holarenamed.txt", "gaby");
        System.out.println("====PERFORMING LS======");
        System.out.println(this.doList("/home/gaby/myDocs"));
        System.out.println("====PERFORMING RMDIR======");
        this.removeDirectory("/home/gaby/myDocs", "gaby");
        System.out.println("====PERFORMING LS======");
        System.out.println(this.doList("/home/gaby"));
        System.out.println("object mapper test");
        System.out.println(new ObjectMapper().writeValueAsString(this.blockMap.get(4)));*/
    }
}
